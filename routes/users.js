var express = require('express');
var MongoClient = require('mongodb').MongoClient;
// Connection URL. This is where our mongodb server is running.
var mongourl = 'mongodb://localhost:27017/my_database_name';

var router = express.Router();

/* GET users listing. */
router.post('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/gethistory', function (req, res, next) {
    var userId = req.body.user_id;
    console.log(userId);
    MongoClient.connect(mongourl, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', mongourl);
            var collection = db.collection('users');
            collection.find({user_id: req.body.user_id}).toArray(function (err, result) {
                if (err) {
                    console.log(err);
                } else if (result.length) {
                    console.log('Found:', result);
                } else {
                    console.log('No document(s) found with defined "find" criteria!');
                }
                //Close connection
                db.close();
                res.send({result: result});
            });
        }
    });
    //res.send(JSON.stringify({data: nodeData}));
});

// First we try to find user by user_id, if we found it we update the existing record, else we insert new record
// we can use save directly but I did it in two steps just in case we need to do any other handling
router.post('/showvideo', function (req, res, next) {
    var userId = req.body.user_id;
    console.log(userId);
    MongoClient.connect(mongourl, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', mongourl);
            var collection = db.collection('users');
            var itemToAdd = {
                image: req.body.image,
                source: req.body.source,
                title: req.body.title,
                description: req.body.description
            };
            collection.find({user_id: req.body.user_id}).toArray(function (err, result) {
                if (err) {
                    console.log(err);
                    db.close();
                } else if (result.length) {
                    // User exist and I'll update it
                    console.log("Item Found in query");
                    // Make Sure that user didn't show this video before by using Image as unique key
                    var exist = false;
                    var history = result[0].history;
                    for (var i = 0; i < history.length; i++) {
                        if (history[i].image == req.body.image) {
                            exist = true;
                            db.close();
                            break;
                        }
                    }
                    if (!exist) {
                        // video not exist and wil add it to history
                        history.push(itemToAdd);
                        console.log('------------ history -----------------');
                        console.log(history);
                        collection.update({user_id: req.body.user_id}, {$set: {"history": history}}, function (err, numUpdated) {
                            if (err) {
                                console.log(err);
                            } else if (numUpdated) {
                                console.log('Updated Successfully %d document(s).', numUpdated);
                            } else {
                                console.log('No document found with defined "find" criteria!');
                            }
                            db.close();
                        });
                    }
                } else {
                    // User not exist and will insert new one
                    console.log("Item Not Found");
                    var user = {
                        user_id: req.body.user_id,
                        history: [itemToAdd]
                    };
                    collection.insert([user], function (err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                        }
                        db.close();
                    });
                }
                //Close connection
                res.send({done: "done"});
            });
        }
    });
});

router.get('/clearhistory', function (req, res, next) {
    // TODO clear history home

    res.send('done');
});

module.exports = router;
