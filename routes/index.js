var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Accedo UI Test' });
});

router.get('/login', function(req, res, next) {
    res.render('login/login', { title: 'Accedo UI Test' });
});

router.get('/dashboard', function(req, res, next) {
    res.render('dashboard/dashboard', { title: 'Accedo UI Test' });
});

router.get('/settings', function(req, res, next) {
    res.render('dashboard/settings/settings', { title: 'Accedo UI Test' });
});

router.get('/home', function(req, res, next) {
    res.render('dashboard/home/home', { title: 'Accedo UI Test' });
});

router.get('/history', function(req, res, next) {
    res.render('dashboard/history/history', { title: 'Accedo UI Test' });
});

router.get('/base', function(req, res, next) {
    res.render('base', { title: 'Accedo UI Test' });
});

module.exports = router;
