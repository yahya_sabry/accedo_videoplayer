/**
 * Created by yahya on 8/20/15.
 */
//Login Controller that manage login view logic, we can use it to authorize and authenticate users to our dashboard and validate emails
myDashApp.controller("LoginController", ["$scope", "$state", "localStorageService", function ($scope, $state, localStorageService) {
    //submit method called when user click login, I didn't added any logic for it just for your simplicity,
    // we can control user authentication, email validation, and any such operations '
    $scope.submit = function () {
        userId = $('#emailValue').val();
        console.log(userId);
        // Save userID in localstorage
        if (userId != ""){
            localStorageService.set('accedo-videoplayer-userID', userId);
            $state.go('home', {});
        }
    }
}]);

var userId;