/**
 * Created by yahya on 8/20/15.
 */
//Dashboard controller where we can control main dashboard views logic, home view and history view
myDashApp.controller("DashboardController", ["$scope", "$state", "localStorageService",function ($scope, $state, localStorageService) {
    $scope.$state = $state;
    // I set only one scope variable to use in Dashboard to show that
    // we can load users data (i.e. User home info, history, settings or any other related data)
    $scope.userName = "Yahya Sabry";
}]);

// public loadedVideos Array
var loadedVideos;
// public method to play video once I click on it's image, It takes Index
var playVideo = function (index) {
    console.log("Image Clicked");
    console.log(loadedVideos[index].contents[0].url);

    $('.videoContainer').show();
    var video = $('.videoContainer')[0];

    var src = {
        src: loadedVideos[index].contents[0].url,
        type: 'video/mp4; codecs=avc1.42E01E, mp4a.40.2"'
    };

    // set video source and type to load and play
    var source = document.createElement('source');
    source.type = src.type;
    source.src = src.src;

    video.appendChild(source);
    video.load();
    video.play();

    // add event listener to video end to hide the video player or to play the next video as I have current video index
    $(".videoContainer").bind("ended", function () {
        console.log('video Ended');
        $('.videoContainer').hide();
    });
    //----------------------------------------------------------
    // load userID from browser in case of refresh
    var userIDToLoad = userId;
    if (userIDToLoad == undefined || userIDToLoad == ""){
        userIDToLoad = localStorageService.get('accedo-videoplayer-userID');
    }
    // use temporary value if userI not found
    if (userIDToLoad == undefined || userIDToLoad == "") {
        userIDToLoad = "guestUser";
    }
    $.ajax({
        url: '/user/showvideo',
        type: "POST",
        dataType: "json",
        data: $.param({
            user_id: userIDToLoad,
            title: loadedVideos[index].title,
            description: loadedVideos[index].description,
            image: loadedVideos[index].images[0].url,
            source: loadedVideos[index].contents[0].url
        }),
        success: function (data) {
            console.log("Success");
            console.log(data);
        },
        error: function (error) {
            console.log('error');
            console.log(error);
        }
    });
}
