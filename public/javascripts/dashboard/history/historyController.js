/**
 * Created by yahya on 8/20/15.
 */
//Dashboard controller where we can control main dashboard views logic, home view and history view
myDashApp.controller("HistoryController", ["$scope", "$state", "localStorageService", function ($scope, $state, localStorageService) {
    $scope.$state = $state;
    // I set only one scope variable to use in Dashboard to show that
    // we can load users data (i.e. User home info, history, settings or any other related data)
    $scope.userName = "Yahya Sabry";
    $('.videoContainer').hide();
    // load userID from browser in case of refresh
    var userIDToLoad = userId;
    if (userIDToLoad == undefined || userIDToLoad == ""){
        userIDToLoad = localStorageService.get('accedo-videoplayer-userID');
    }
    // use temporary value if userI not found
    if (userIDToLoad == undefined || userIDToLoad == "") {
        userIDToLoad = "guestUser";
    }
    //----------------------------------------------------------
    $.ajax({
        url: '/user/gethistory',
        type: "POST",
        dataType: "json",
        data: $.param({
            user_id: userIDToLoad
        }),
        success: function (data) {
            console.log("Success");
            console.log(data);
                    // reset loaded Videos array
                    loadedVideos = [];
                    // parse loaded videos items just like what it expect from out API
                    var historyLoadedVideos = data.result[0].history;
                    for (var i = 0; i < historyLoadedVideos.length; i++) {
                        loadedVideos.push({
                            title: historyLoadedVideos[i].title,
                            description: historyLoadedVideos[i].description,
                            images: [{url: historyLoadedVideos[i].image}],
                            contents: [{url: historyLoadedVideos[i].source}]
                        });
                    }
                    // Create Div Element that contains slide object
                    var iDiv = document.createElement('div');
                    iDiv.id = 'block';
                    iDiv.className = 'block';
                    for (var i = 0; i < loadedVideos.length; i++) {
                        var div = '<div id= ' + loadedVideos[i].images[0].id + ' class="loading slide">';
                        $('<img>').appendTo($(div).appendTo($('.jumbotron'))).one('load', function () {
                            $(this).parent().removeClass('loading');
                        }).attr('src', loadedVideos[i].images[0].url)
                            .attr('onclick', 'playVideo(' + i + ')');
                    }
                    // set up boxslider with needed values
                    $('.jumbotron').bxSlider({
                        slideWidth: 250,
                        minSlides: 2,
                        maxSlides: 3,
                        moveSlides: 3,
                        slideMargin: 10
                    });

        },
        error: function (error) {
            console.log('error');
            console.log(error);
        }

    });
}]);