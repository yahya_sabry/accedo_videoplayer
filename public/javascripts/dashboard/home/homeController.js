/**
 * Created by yahya on 8/20/15.
 */
//Dashboard controller where we can control main dashboard views logic, home view and history view
myDashApp.controller("HomeController", ["$scope", "$state", function ($scope, $state) {
    $scope.$state = $state;
    // I set only one scope variable to use in Dashboard to show that
    // we can load users data (i.e. User home info, history, settings or any other related data)
    $scope.userName = "Yahya Sabry";
    $('.videoContainer').hide();

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://demo2697834.mockable.io/movies", true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            loadedVideos = JSON.parse(xhr.responseText).entries;
            var title = "";
            var image = "";
            var source = "";
            var description = "";

            // Create Div Element that contains slide object
            var iDiv = document.createElement('div');
            iDiv.id = 'block';
            iDiv.className = 'block';
            for (var i = 0; i < loadedVideos.length; i++) {
                title = loadedVideos[i].title;
                description = loadedVideos[i].description;
                image = loadedVideos[i].images[0].url;
                source = loadedVideos[i].contents[0].url;
                console.log("Title = ", title);
                console.log("description = ", description);
                console.log("image = ", image);
                console.log("source = ", source);
                var div = '<div id= ' + loadedVideos[i].images[0].id + ' class="loading slide">';

                $('<img>').appendTo($(div).appendTo($('.jumbotron'))).one('load', function () {
                    $(this).parent().removeClass('loading');
                }).attr('src', loadedVideos[i].images[0].url)
                    .attr('onclick', 'playVideo(' + i + ')');
            }
            // set up boxslider with needed values
            $('.jumbotron').bxSlider({
                slideWidth: 250,
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 3,
                slideMargin: 10
            });
        }
    }
    xhr.send();
}]);
