"use strict";
// Intialize the main Angular "myDashApp", which has three dependencies ["ui.router", "ngAnimate", "LocalStorageModule"]
// ui.router: to manage the applications routes
// ngAnimate: to applay simple animation while transaction between views
// LocalStorageModule: to manipulate with browser storage
var myDashApp = angular.module("myDashApp", ["ui.router", "ngAnimate", "LocalStorageModule"]);
//var myDashApp = angular.module("myDashApp", ["ui.router"]);

console.log("Here");
console.log(myDashApp);

myDashApp.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
    // configure the app to have the main landing page "Video page"
    // otherwise, user redirect to login page
    console.log("Here");
    $urlRouterProvider.when("/dashboard", "settings"),
        $urlRouterProvider.otherwise("/login");

    // configure the main route, views and their controllers
    $stateProvider.state("base", {
        "abstract": !0,
        url: "",
        templateUrl: "/base"
    }).state("login", {
        url: "/login",
        parent: "base",
        templateUrl: "/login",
        controller: "LoginController"
    }).state("dashboard", {
        url: "/dashboard",
        parent: "base",
        templateUrl: "/dashboard",
        controller: "DashboardController"
    }).state("history", {
        url: "/history",
        parent: "dashboard",
        templateUrl: "/history",
        controller: "HistoryController"
    }).state("home", {
        url: "/home",
        parent: "dashboard",
        templateUrl: "/home",
        controller: "HomeController"
    }).state("settings", {
        url: "/settings",
        parent: "dashboard",
        templateUrl: "/settings",
        controller: "SettingsController"
    });
}]);
